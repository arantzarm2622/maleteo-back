require("dotenv").config();
const express = require("express");
const passport = require("passport");
const cors = require("cors");
const mongoose = require("mongoose");
const session = require("express-session");
const MongoStore = require("connect-mongo");
const userRouter = require("./routes/user.routes");
const spaceRouter = require("./routes/space.routes");

const db = require("./db.js");
db.connect();

require("./passport/passport");
const PORT = process.env.PORT || 3000;
const server = express();

server.use((req, res, next) => {
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
  res.header("Access-Control-Allow-Credentials", true);
  res.header("Access-Control-Allow-Headers", "Content-Type");
  next();
});

server.use(
  cors({
    origin: ["http://localhost:3000"],
    credentials: true,
  })
);

server.use(
  session({
    secret: process.env.SESSION_SECRET_KEY,
    resave: false,
    saveUninitialized: false,
    cookie: {
      maxAge: 48 * 60 * 60 * 1000,
      httpOnly: false,
      secure: false,
      sameSite: false,
    },
    store: MongoStore.create({ mongoUrl: db.DB_URL }),
  })
);

server.use(passport.initialize());
server.use(passport.session());

server.use(express.json());
server.use(express.urlencoded({ extended: true }));

server.use("/auth", userRouter);
server.use("/space", spaceRouter);

server.use("*", (req, res, next) => {
  const error = new Error("Route not found");
  error.status = 404;
  next(error);
});

server.use((error, req, res, next) => {
  return res
    .status(error.status || 500)
    .json({ message: error.message || "Unexpected error" });
});

server.listen(PORT, () => {
  console.log(`Server started on http://localhost:${PORT}`);
});
