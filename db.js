const mongoose = require("mongoose");

const DB_URL = process.env.DB_URL;

const connect = async () => {
  try {
    const connection = await mongoose.connect(DB_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });

    const { name, host } = connection.connections[0];

    console.log(`Connected to DB ${name} at ${host}`);
  } catch (error) {
    console.log("Error connecting to database", error);
  }
};

module.exports = {
  connect,
  DB_URL,
};
