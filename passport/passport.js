const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const bcrypt = require("bcrypt");
const User = require("../models/User");

const SALT_ROUNDS = 10;

passport.serializeUser((user, done) => {
  return done(null, user._id);
});

passport.deserializeUser(async (userId, done) => {
  try {
    const existingUser = await User.findById(userId);
    return done(null, existingUser);
  } catch (err) {
    return done(err);
  }
});

const registerStrategy = new LocalStrategy(
  {
    usernameField: "email",
    passwordField: "password",
    passReqToCallback: true,
  },
  async (req, email, password, done) => {
    try {
      const previousUser = await User.findOne({ email });
      if (previousUser) {
        const error = new Error("Datos no válidos");
        return done(error);
      }

      const { firstName, lastName, birthDate } = req.body;
      let wantInfo = false;
      if (req.body.wantInfo) {
        wantInfo = true;
      }
      const passwordHash = await bcrypt.hash(password, SALT_ROUNDS);

      const newUser = new User({
        firstName,
        lastName,
        email,
        password: passwordHash,
        birthDate,
        wantInfo,
      });

      const savedUser = await newUser.save();

      return done(null, savedUser);
    } catch (error) {
      return done(error);
    }
  }
);

const loginStrategy = new LocalStrategy(
  {
    usernameField: "email",
    passwordField: "password",
    passReqToCallback: true,
  },
  async (req, email, password, done) => {
    try {
      const currentUser = await User.findOne({ email });

      if (!currentUser) {
        const error = new Error("El email y/o la contraseña no son válidos");
        return done(error);
      }

      const isValidPassword = await bcrypt.compare(
        password,
        currentUser.password
      );

      if (!isValidPassword) {
        const error = new Error("El email y/o la contraseña no son válidos");
        return done(error);
      }

      done(null, currentUser);
    } catch (err) {
      return done(err);
    }
  }
);

passport.use("registration", registerStrategy);
passport.use("login", loginStrategy);
