const passport = require("passport");
const Space = require("../models/Space");

const spaceListGet = async (req, res, next) => {
  try {
    const spaceList = await Space.find();
    return res.json(spaceList);
  } catch (error) {
    return res.json({ message: "Se ha producido un error" });
  }
};

const mySpaceListGet = async (req, res, next) => {
  try {
    const guardian = req.user;
    const spaceList = await Space.find({ guardian });
    return res.json(spaceList);
  } catch (error) {
    return res.json({ message: "Se ha producido un error" });
  }
};

const spaceByIdGet = async (req, res, next) => {
  try {
    const { id } = req.params;
    const space = await Space.findById(id).populate('guardian');

    if (space) {
      return res.json(space);
    }

    return res.json({ message: "Se ha producido un error" });
  } catch (error) {
    return res.json({ message: "Se ha producido un error" });
  }
};

const newSpacePost = async (req, res, next) => {
  try {
    const guardian = req.user;
    const {
      name,
      propertyType,
      spaceType,
      description,
      address,
      city,
      province,
      country,
      availability,
      timetable,
    } = req.body;

    let images = "";
    if (req.file) {
      images = req.file.filename;
    }

    const newSpace = new Space({
      guardian,
      name,
      propertyType,
      spaceType,
      description,
      images,
      address,
      city,
      province,
      country,
      availability,
      timetable,
    });
    await newSpace.save();

    return res.status(200).json(newSpace);
  } catch (error) {
    return res.json({ message: "Se ha producido un error" });
  }
};

module.exports = {
  spaceListGet,
  mySpaceListGet,
  spaceByIdGet,
  newSpacePost,
};
