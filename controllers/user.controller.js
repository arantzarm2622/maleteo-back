const passport = require("passport");
const User = require("../models/User");

const registerPost = (req, res, next) => {
  const { firstName, lastName, email, password, birthDate } = req.body;

  if (!firstName || !lastName || !email || !password || !birthDate) {
    return res
      .status(400)
      .json({ message: "Todos los campos son obligatorios" });
  }

  passport.authenticate("registration", (error, user) => {
    if (error) {
      // return res.status(403).json({ message: error.message });
      return res.json({ message: error.message });
    }

    req.logIn(user, (error) => {
      if (error) {
        return res.json({ message: error.message });
      }

      let registeredUser = user;
      registeredUser.password = null;

      return res.status(200).json(registeredUser);
    });
  })(req);
};

const loginPost = (req, res, next) => {
  const { email, password } = req.body;

  if (!email || !password) {
    // return res
    //   .status(400)
    //   .json({ message: "Los campos email y contraseña son obligatorios" });
    return res.json({
      message: "Los campos email y contraseña son obligatorios",
    });
  }

  passport.authenticate("login", (error, user) => {
    if (error) {
      return res.json({ message: error.message });
    }

    req.logIn(user, (error) => {
      if (error) {
        return res.json({ message: error.message });
      }

      let loggedUser = user;
      loggedUser.password = null;

      return res.status(200).json(loggedUser);
    });
  })(req, res, next);
};

const logoutPost = (req, res, next) => {
  if (req.user) {
    req.logout();

    req.session.destroy(() => {
      res.clearCookie("connect.sid");
      return res.status(200).json("El usuario se ha deslogueado correctamente");
    });
  } else {
    // return res.status(401).json({ message: "Usuario no encontrado" });
    return res.json({ message: "Usuario no encontrado" });
  }
};

const checkSessionGet = async (req, res, next) => {
  if (req.user) {
    let registeredUser = req.user;
    registeredUser.password = null;

    return res.status(200).json(registeredUser);
  } else {
    // return res.status(401).json({ message: "Usuario no encontrado" });
    return res.json({ message: "Usuario no encontrado" });
  }
};


const becomeGuardianPost = async (req, res, next) => {
  try {
    const { id } = req.body;

    const updatedUser = await User.findByIdAndUpdate(
      id,
      { $set: { role: "guardian" } },
      { new: true, useFindAndModify: false }
    );

    return res.status(200).json(updatedUser);
  } catch (error) {
    return res.json({ message: "Usuario no encontrado" });
  }
};

module.exports = {
  registerPost,
  loginPost,
  logoutPost,
  checkSessionGet,
  becomeGuardianPost,
};
