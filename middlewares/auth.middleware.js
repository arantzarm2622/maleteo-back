const isAuthenticated = (req, res, next) => {
  if (req.isAuthenticated()) {
    return next();
  } else {
    return res.send(403);
  }
};

const isGuardian = (req, res, next) => {
  if (req.user.role == "guardian") {
    return next();
  } else {
    return res.send(403);
  }
};

module.exports = { isAuthenticated, isGuardian };
