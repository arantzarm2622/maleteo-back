const express = require("express");
const spaceController = require("../controllers/space.controller");
const {isAuthenticated, isGuardian} = require('../middlewares/auth.middleware');

const router = express.Router();

router.post("/new", isGuardian, spaceController.newSpacePost);
router.get("/my-spaces", isGuardian, spaceController.mySpaceListGet);
router.get("/:id", isAuthenticated, spaceController.spaceByIdGet);
router.get("/", isAuthenticated, spaceController.spaceListGet);

module.exports = router;
