const express = require("express");
const userController = require("../controllers/user.controller");
const {isAuthenticated, isGuardian} = require('../middlewares/auth.middleware');

const router = express.Router();

router.post("/register", userController.registerPost);
router.post("/login", userController.loginPost);
router.post("/logout",isAuthenticated,  userController.logoutPost);
router.get("/check-session", isAuthenticated, userController.checkSessionGet);
router.post("/become-guardian", isGuardian, userController.becomeGuardianPost);

module.exports = router;
