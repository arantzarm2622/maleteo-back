const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSchema = new Schema(
  {
    email: { type: String, required: true },
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    password: { type: String, required: true },
    birthDate: { type: Date, required: true },
    userImage: { type: String },
    wantInfo: { type: Boolean, default: false, required: true },
    role: {
      enum: ["guardian", "traveler"],
      type: String,
      default: "traveler",
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

const User = mongoose.model("User", userSchema);

module.exports = User;
