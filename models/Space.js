const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSchema = new Schema(
  {
    guardian: { type: mongoose.Types.ObjectId, ref: "User" },
    name: { type: String, required: true },
    propertyType: {
      enum: ["Casa", "Hotel", "Establecimiento"],
      type: String,
      required: true,
    },
    spaceType: {
      enum: ["Habitación", "Hall", "Trastero", "Buhardilla", "Garaje"],
      type: String,
      required: true,
    },   
    description: { type: String, required: true },
    images: [{ type: String }],
    address: { type: String, required: true },
    city: { type: String, required: true },
    province: { type: String, required: true },
    country: { type: String, required: true },
    availability: { type: String },
    timetable: { type: String },
  },
  {
    timestamps: true,
  }
);

const Space = mongoose.model("Space", userSchema);

module.exports = Space;
